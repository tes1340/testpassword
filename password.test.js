const { checkLength, checkAlphabet, checkSymbol, checkDigit, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be ture', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has not alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has Symbol ! in password to be true', () => {
    expect(checkSymbol('!')).toBe(true)
  })

  test('should has Symbol @ in password to be true', () => {
    expect(checkSymbol('11@11')).toBe(true)
  })
})

describe('Test Digit', () => {
  test('should has digit to be true', () => {
    expect(checkDigit('phai12345')).toBe(true)
  })

  test('should has not digit password to be true', () => {
    expect(checkDigit('iloveu')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password @Boy12 to be false', () => {
    expect(checkPassword('@Boy12')).toBe(false)
  })

  test('should password to be false', () => {
    expect(checkPassword('123456789')).toBe(false)
  })

  test('should password @boy123456 to be true', () => {
    expect(checkPassword('@boy123456')).toBe(true)
  })
})
